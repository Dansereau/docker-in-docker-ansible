FROM docker:latest

USER root

RUN apk add gcc git libffi-dev musl-dev \
    openssl-dev perl py-pip python python-dev \
    sshpass make sudo openssh yarn npm

RUN sudo pip install --upgrade pip

RUN sudo pip install git+git://github.com/ansible/ansible.git@devel

CMD ["wrapdocker"]
